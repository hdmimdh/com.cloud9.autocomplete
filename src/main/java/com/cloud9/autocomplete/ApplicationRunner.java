package com.cloud9.autocomplete;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@EnableScheduling
@PropertySource({
        "./connection.properties"
})
public class ApplicationRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationRunner.class);

    public static void main(final String[] args) throws UnknownHostException {

        final Environment env = SpringApplication.run(ApplicationRunner.class, args).getEnvironment();

        LOGGER.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\thttp://localhost:{}{}{}\n\t" +
                        "External: \thttp://{}:{}{}{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"), env.getProperty("server.context-path"), "/swagger-ui.html#/",
                InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"),
                env.getProperty("server.context-path"), "/swagger-ui.html#/");
    }
}
