package com.cloud9.autocomplete.utils;

import org.apache.lucene.analysis.CharArraySet;

import java.util.Arrays;

public final class QueryUtils {

    public static final String ELEMENT_TABLE = "element_table";
    public static final String ELEMENT_COLUMN = "element_column";
    public static final String ELEMENT_COLUMN_VALUE = "element_value";
    public static final String ELEMENT_KEY_WORD = "element_key";

    public static final String TABLE_ATTR = "TABLE";
    public static final String TABLE_NAME_ATTR = "TABLE_NAME";
    public static final String COLUMN_NAME_ATTR = "COLUMN_NAME";
    public static final String COLUMN_TYPE_NAME_ATTR = "TYPE_NAME";

    public static final String DOCUMENT_TITLE = "title";
    public static final String DOCUMENT_TYPE = "type";
    public static final String DOCUMENT_TABLE_NAME = "tableName";
    public static final String DOCUMENT_COLUMN_NAME = "columnName";
    public static final String DOCUMENT_COLUMN_TYPE = "columnType";
    public static final String DOCUMENT_COLUMN_VALUE = "columnValue";

    private static final String[] STOP_WORDS = {
            "a", "an", "and", "are", "as", "at", "be", "but", "by",
            "for", "i", "if", "in", "into", "is",
            "no", "not", "of", "on", "or", "s", "such",
            "t", "that", "the", "their", "then", "there", "these",
            "they", "this", "to", "was", "will", "with"
    };
    public static final CharArraySet ENGLISH_STOP_WORDS = new CharArraySet(Arrays.asList(STOP_WORDS), true);

    private QueryUtils() {
        throw new AssertionError();
    }
}
