package com.cloud9.autocomplete.controller;

import com.cloud9.autocomplete.ApplicationRunner;
import com.cloud9.autocomplete.service.LuceneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.lucene.store.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.util.List;

@RestController
@RequestMapping("/lucene")
@Api(value = "/lucene", description = "Lucene resource.")
public class LuceneController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationRunner.class);

    private static final int MAX_DOCUMENTS_LIMIT = 10;

    private Directory index;

    @Value("${db.connection.url}")
    private String dbconnectionUrl;

    @Value("${db.connection.user}")
    private String dbconnectionUser;

    @Value("${db.connection.password}")
    private String dbconnectionPassword;

    private final LuceneService luceneService;

    @Autowired
    public LuceneController(LuceneService luceneService) {
        this.luceneService = luceneService;
    }

    @ApiOperation(value = "Creates lucene createIndex.", nickname = "createLuceneDirectoryIndex")
    @RequestMapping(value = "/index", method = RequestMethod.POST)
    public void createLuceneDirectoryIndex() {

        LOGGER.info("Creating lucene index createIndex");

        final Connection connection = luceneService.createConnection(dbconnectionUrl, dbconnectionUser, dbconnectionPassword);
        this.index = luceneService.createIndex(connection);

        LOGGER.info("Lucene createIndex created successfully");
    }

    @ApiOperation(value = "Suggests by query.", nickname = "suggestByQuery")
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public List<String> suggestByQuery(@RequestParam(value = "query") final String query) {

        if (this.index == null) {
            createLuceneDirectoryIndex();
        }

        LOGGER.info("Suggesting for query {}", query);
        return luceneService.suggestByQuery(index, query, MAX_DOCUMENTS_LIMIT);
    }

    @ApiOperation(value = "Releases index.", nickname = "releaseIndex")
    @RequestMapping(value = "/release", method = RequestMethod.POST)
    public void releaseLuceneIndex() {

        LOGGER.info("Releasing lucene index");
        luceneService.releaseIndex(index);
        this.index = null;
    }
}
