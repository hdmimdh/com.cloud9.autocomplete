package com.cloud9.autocomplete.configuration;

public final class SwaggerConstants {

    public static final String OK_RESPONSE = "Success";
    public static final String BAD_REQUEST = "Bad Request";
    public static final String UNAUTHORIZED = "Unauthorized";
    public static final String FORBIDDEN = "Forbidden";
    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";

    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_PUT = "PUT";
    public static final String METHOD_DELETE = "DELETE";

    private SwaggerConstants() {
        throw new AssertionError();
    }
}
