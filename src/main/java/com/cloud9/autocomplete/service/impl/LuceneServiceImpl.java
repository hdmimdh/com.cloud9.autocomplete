package com.cloud9.autocomplete.service.impl;

import com.cloud9.autocomplete.service.LuceneService;
import com.cloud9.autocomplete.utils.QueryUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.ngram.EdgeNGramTokenFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queries.CustomScoreQuery;
import org.apache.lucene.queries.function.FunctionQuery;
import org.apache.lucene.queries.function.valuesource.LongFieldSource;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class LuceneServiceImpl implements LuceneService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuceneServiceImpl.class);

    private static final Long LIMIT = 2000L;

    @Override
    public Connection createConnection(final String url, final String user, final String password) {

        LOGGER.warn("Creating db connection {}", url);

        if (StringUtils.isBlank(url) || StringUtils.isBlank(user) || StringUtils.isBlank(password)) {
            LOGGER.warn("Credentials information is missing url: {}, user: {}, password: {}", url, user, password);
            return null;
        }

        try {

            final Connection connection = DriverManager.getConnection(url, user, password);
            LOGGER.info("Db connection {} successfully created", url);
            return connection;

        } catch (Throwable throwable) {

            LOGGER.error("Failed to create db connection {} due to {}", url, throwable.getMessage());
            return null;
        }
    }

    @Override
    public Directory createIndex(final Connection connection) {

        try {

            LOGGER.info("Creating lucene index on db connection");

            final Analyzer analyzer = createAnalyzer();
            final Directory index = new RAMDirectory();
            final IndexWriterConfig config = new IndexWriterConfig(analyzer);

            final DatabaseMetaData dbmd = connection.getMetaData();
            try (
                    final ResultSet tables = dbmd.getTables(null, "public", "%", new String[]{"TABLE"});
                    final IndexWriter writer = new IndexWriter(index, config)) {

                while (tables.next()) {

                    final String tableName = tables.getString("TABLE_NAME");
                    addDocumentTable(writer, tableName);

                    final List<String> columns = new ArrayList<>();

                    try (
                            final ResultSet rsColumn = dbmd.getColumns(null, null, tableName, null)) {

                        while (rsColumn.next()) {

                            final String columnName = rsColumn.getString(QueryUtils.COLUMN_NAME_ATTR);
                            final String columnType = rsColumn.getString(QueryUtils.COLUMN_TYPE_NAME_ATTR);
                            addDocumentColumn(writer, tableName, columnName, columnType);
                            columns.add(columnName);
                        }
                    }

                    final String query = "SELECT * FROM " + tableName + " ORDER BY RANDOM() LIMIT " + LIMIT + ";";

                    try (
                            final Statement stmt = connection.createStatement();
                            final ResultSet rows = stmt.executeQuery(query)) {

                        while (rows.next()) {

                            for (int i = 0; i < columns.size(); i++) {

                                String columnName = columns.get(i);
                                String columnValue = rows.getString(i + 1);
                                addDocumentColumnValue(writer, tableName, columnName, columnValue);
                            }
                        }
                    }
                }
            }
            LOGGER.info("Lucene index on db connection successfully created");
            return index;

        } catch (Throwable throwable) {

            LOGGER.error("Failed to create lucene index due to ", throwable);
            return null;
        }
    }

    @Override
    public void releaseIndex(final Directory index) {

        if (index == null) {
            return;
        }

        try {
            index.close();
        } catch (Throwable throwable) {
            LOGGER.error("Failed to release index due to", throwable);
        }
    }

    private void addDocumentTable(final IndexWriter writer, String tableName) throws IOException {

        final Document doc = new Document();

        doc.add(new TextField(QueryUtils.DOCUMENT_TITLE, tableName, Field.Store.YES));
        doc.add(new StringField(QueryUtils.DOCUMENT_TYPE, QueryUtils.ELEMENT_TABLE, Field.Store.YES));
//        doc.add(new NumericDocValuesField("boost", 1L));

        writer.addDocument(doc);
    }

    private void addDocumentColumn(final IndexWriter writer, String tableName, String columnName, String columnType) throws IOException {

        final Document doc = new Document();

        doc.add(new TextField(QueryUtils.DOCUMENT_TITLE, columnName, Field.Store.YES));
        doc.add(new StringField(QueryUtils.DOCUMENT_TYPE, QueryUtils.ELEMENT_COLUMN, Field.Store.YES));

        doc.add(new StringField(QueryUtils.DOCUMENT_TABLE_NAME, tableName, Field.Store.YES));
        doc.add(new StringField(QueryUtils.DOCUMENT_COLUMN_NAME, columnName, Field.Store.YES));
        doc.add(new StringField(QueryUtils.DOCUMENT_COLUMN_TYPE, columnType, Field.Store.YES));
//        doc.add(new NumericDocValuesField("boost", 1L));

        writer.addDocument(doc);
    }

    private void addDocumentColumnValue(final IndexWriter writer, String tableName, String columnName, String columnValue) throws IOException {

        final Document doc = new Document();

        doc.add(new TextField(QueryUtils.DOCUMENT_TITLE, columnValue, Field.Store.YES));
        doc.add(new StringField(QueryUtils.DOCUMENT_TYPE, QueryUtils.ELEMENT_COLUMN_VALUE, Field.Store.YES));

        doc.add(new StringField(QueryUtils.DOCUMENT_TABLE_NAME, tableName, Field.Store.YES));
        doc.add(new StringField(QueryUtils.DOCUMENT_COLUMN_NAME, columnName, Field.Store.YES));
        doc.add(new StringField(QueryUtils.DOCUMENT_COLUMN_VALUE, columnValue, Field.Store.YES));
//        doc.add(new NumericDocValuesField("boost", 2L));

        writer.addDocument(doc);
    }

    @Override
    public List<String> suggestByQuery(final Directory index, final String querystr, final int limit) {

        try {

            try (final IndexReader reader = DirectoryReader.open(index)) {

                final Analyzer analyzer = createAnalyzer();
                final Query baseQuery = new QueryParser(QueryUtils.DOCUMENT_TITLE, analyzer).parse(querystr);
//                final FunctionQuery boostQuery = new FunctionQuery(new LongFieldSource("boost"));
//                final Query q = new CustomScoreQuery(baseQuery, boostQuery);

                final IndexSearcher searcher = new IndexSearcher(reader);
                final TopDocs documents = searcher.search(baseQuery, limit);
                final ScoreDoc[] scoreDocuments = documents.scoreDocs;
                if (scoreDocuments == null || scoreDocuments.length == 0) {

                    LOGGER.warn("No matches found by query {}", querystr);
                    return Collections.emptyList();
                }

                LOGGER.info("Found {} matches", scoreDocuments.length);

                final List<String> titles = new ArrayList<>();
                for (ScoreDoc scoreDocument : scoreDocuments) {

                    final int docId = scoreDocument.doc;
                    final Document document = searcher.doc(docId);
                    titles.add(document.get(QueryUtils.DOCUMENT_TITLE));
                }
                return titles;
            }

        } catch (Throwable throwable) {

            LOGGER.info("Failed to query lucene index due to ", throwable);
            return Collections.emptyList();
        }
    }

    private Analyzer createAnalyzer() {

        return new Analyzer() {
            @Override
            protected TokenStreamComponents createComponents(String fieldName) {

                final Tokenizer source = new StandardTokenizer();
                TokenStream result = new StandardFilter(source);
                result = new LowerCaseFilter(result);
                result = new StopFilter(result, QueryUtils.ENGLISH_STOP_WORDS);
                result = new EdgeNGramTokenFilter(result, 1, 20);
                return new TokenStreamComponents(source, result);
            }
        };
    }
}
