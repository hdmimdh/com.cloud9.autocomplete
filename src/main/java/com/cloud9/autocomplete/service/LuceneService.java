package com.cloud9.autocomplete.service;

import org.apache.lucene.store.Directory;

import java.sql.Connection;
import java.util.List;

public interface LuceneService {

    Connection createConnection(String url, String user, String password);

    Directory createIndex(Connection connection);

    void releaseIndex(Directory index);

    List<String> suggestByQuery(Directory index, String query, int limit);
}
